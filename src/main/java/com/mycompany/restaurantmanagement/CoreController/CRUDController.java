/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.CoreController;

/**
 *
 * @author Risab
 */
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Risab
 */
public class CRUDController<T> extends SiteController{
    protected String viewPath;
    
    
    
    @Autowired
    protected JpaRepository<T, Integer> repo;
    
    
   @GetMapping()
    public String index(Model model){
        
       model.addAttribute("records", repo.findAll());
        return viewPath+"/index";
    }
    @GetMapping(value = "/json")
    public @ResponseBody List<T> json(){
        return repo.findAll(); 
    }
    
    @GetMapping(value = "/json/{id}")
    public @ResponseBody    T json(@PathVariable("id")int id){
        return repo.findById(id).get();
    }
    
    @GetMapping("/create")
    public String create(){
        return viewPath+"/create";
    }
   @PostMapping()
   public String save(T model){
      repo.save(model);
       return "redirect:";
   }
   @GetMapping(value  ="/edit/{id}")
   public String edit(@PathVariable("id")int id,Model model){
       model.addAttribute("record",repo.findById(id).get());
       return viewPath+"/edit";
   }
   
}
