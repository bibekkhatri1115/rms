/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.CoreController;

/**
 *
 * @author Risab
 */


import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Risab
 */
public abstract class SiteController {
    protected String activeMenu;
    protected String pageURI;
    protected String pageTitle;
    
    @ModelAttribute(value="activeMenu")
    protected String getActiveMenu(){
        return activeMenu;
        
    }
    
    @ModelAttribute(value="pageURI")
    protected String getPageURI(){
        return pageURI;
    }
    
    @ModelAttribute(value="pageTitle")
    protected String getPageTitle(){
        return pageTitle;
    }
}
