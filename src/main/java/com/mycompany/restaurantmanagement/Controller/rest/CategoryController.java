package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.Category;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/category")
public class CategoryController extends CRUDController<Category> {
    
   public CategoryController(){
        activeMenu="FiscalYear";
       viewPath ="category";
       pageURI="category";
   }
    
}

