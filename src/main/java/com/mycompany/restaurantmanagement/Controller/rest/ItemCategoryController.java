package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.ItemCategory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/itemcategory")
public class ItemCategoryController extends CRUDController<ItemCategory> {
    
   public ItemCategoryController(){
        activeMenu="master";
       viewPath ="/ItemCategory";
       pageURI="itemcategory";
   }
    
}

