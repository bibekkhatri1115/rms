    package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.PurchaseRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/purchaserequest")
public class PurchaseRequestController extends CRUDController<PurchaseRequest> {
    
   public PurchaseRequestController(){
        activeMenu="master";
       viewPath ="/PurchaseRequest";
       pageURI="purchaserequest";
   }
    
}

