package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.Unit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/unit")
public class UnitController extends CRUDController<Unit> {
    
   public UnitController(){
        activeMenu="master";
       viewPath ="/unit";
       pageURI="unit";
   }
    
}

