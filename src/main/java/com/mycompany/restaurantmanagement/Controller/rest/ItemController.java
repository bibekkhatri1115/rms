package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.Item;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/item")
public class ItemController extends CRUDController<Item> {
    
   public ItemController(){
        activeMenu="master";
       viewPath ="/item";
       pageURI="item";
   }
    
}

