package {{packageName}}Controller.rest;

import  {{packageName}}CoreController.CRUDController;
import  {{packageName}}entity.{{Entity}};
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/{{url}}")
public class {{Entity}}Controller extends CRUDController<{{Entity}}> {
    
   public {{Entity}}Controller(){
        activeMenu="master";
       viewPath ="/{{Entity}}";
       pageURI="{{url}}";
   }
    
}

